#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
//william doyle
/*
 	guide:
	call check_malloc(pointer_you_want_to_check, __func__);
*/
static inline int check_malloc(void * value, const char * funk){
	if (value){
		return 1;
	}else {	/* malloc failed */
		time_t now;
		time(&now);
		fprintf(stderr, "Malloc failed! Function: %s Time: %s\n", funk, ctime(&now));
		FILE * fp = fopen("MALLOC_LOG.txt", "a");
		fprintf(fp, "Malloc failed! Function: %s Time: %s\n", funk, ctime(&now));
		fclose(fp);
		exit(EXIT_FAILURE); 
	}
	return 0;
};
