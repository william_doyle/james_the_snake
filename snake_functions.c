#include "snake.h"
// functions specific to the snake
// constructors, destructors, actions (mainly growing snake)
// author William Doyle
// Febuary 26th 2020
// snake_functions.c

void move_part(struct snake_part * part, struct point newplace ){
	
	extern char * field[__SIZE][__SIZE];
	field[newplace.x][newplace.y] = BLACK_SQUARE;

	struct point prevPoint = part->place;
	part->place = newplace;
	if (part->next != NULL){
		move_part(part->next , prevPoint);
	}
	return;
}

void move_snake (struct snake * self){
	/*	Based on the value of 'direction' increment or decrement a clone of the head.
	 	Then move the head to this new position by calling move_part. move_part will 
		recursivly move the remaining snake_parts and their new position. 	
	*/
	extern char direction;
	struct point nhp = self->head->place;
	switch (direction){
		case 'N':
			nhp.x--;
			break;
		case 'E':
			nhp.y++;
			break;
		case 'S':
			nhp.x++;
			break;
		case 'W':
			nhp.y--;
			break;
		default:
			break;
	};
	
	if (nhp.y >= 50 ){ nhp.y = 1; }/* check to see if snake is going out of bounds. if so wrap around */
	if (nhp.y < 0 ){ nhp.y = __SIZE-1;}
	if (nhp.x >= __SIZE ){ nhp.x = 1; }
	if (nhp.x < 0  ){ nhp.x = __SIZE-1;}

	move_part(self->head, nhp);
}

struct snake * make_snake(int x, int y){
	struct snake * the_snake;
	the_snake = malloc(sizeof(struct snake));
	check_malloc(the_snake, __func__);
	the_snake->head = make_snake_part(x,y);
	the_snake->move = move_snake;
	return the_snake;
}

struct snake_part * make_snake_part(int x, int y){
	struct snake_part * the_part;
	the_part = malloc(sizeof(struct snake_part));
	check_malloc(the_part, __func__);
	the_part->next = NULL;/* avoid segmentation fault */
	the_part->place.x = x;
	the_part->place.y = y;
	return the_part;
}

void grow_snake(struct snake * self){
	add_part(self->head);
}

void recursive_snake_destroy(struct snake_part * part){
	if (part->next == NULL){
		free(part);
	}else{
		recursive_snake_destroy(part->next);
		free(part);
	}
}

void destroy_snake(struct snake * self){
	recursive_snake_destroy(self->head);
	free(self);
}

void add_part(struct snake_part * part){
	extern char direction;
	if (part->next == NULL){
		/* fine to generate part at this position because of how the "follow" mechanic was implemented */
		part->next = make_snake_part(part->place.x, part->place.y); 
	}else{
		add_part(part->next);
	}
	return;
}



