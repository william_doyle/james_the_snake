#pragma once

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <GL/freeglut.h>
#include <locale.h>
#include "check_malloc.h"
#include <ncurses.h>

#define BLACK_SQUARE "\u25A0"
#define WHITE_SQUARE "\u25A1"
#define FOOD_ICON    "\u272A"
#define __SIZE 50

// #define NO_SOUNDS
#ifdef NO_SOUNDS
	#define NO_MUSIC
	#define NO_SFX
#endif
//#define NO_MUSIC

/* A location with x any y cordinants */
char * field[__SIZE][__SIZE];
int food_pos[2];
unsigned short apple_type;
unsigned short apple_shade_extent;

// things to eventualy not have external 
unsigned sleep_time;
unsigned player_score;
struct settings _settings;
struct snake * james;
struct point * food;
void (*show_all)(char * [__SIZE][__SIZE]);
char direction; // N E S W {north east south west}

struct point {
       	int x;
       	int y;
};

/* a segment of a snake */
struct snake_part {
	struct point place;
	struct snake_part * next;
};

/* a wraper for a snake --snake is a lot like a single linked list */
struct snake {
	struct snake_part * head;
	void (*move)(struct snake *);
};

/* settings wrapper */
struct settings {
	unsigned short hardness;
	unsigned short graphics_mode;

};

/* display settings menu and return chosen settings */
struct settings game_menu(void);

void s_key_pressed(int , int , int );

// --------------------     SNAKE_FUNCTIONS.C
	/* move a snake part to a new point */
	void move_part(struct snake_part *, struct point);

	/* move the snake in a global direction.. recursive call move_part */
	void move_snake(struct snake *);

	/* generate a new part of the snake with its on memory allocation */
	struct snake_part * make_snake_part(int, int); // should be called make_snake_part for consistency

	/* make a new snake and return pointer to it */
	struct snake * make_snake(int, int);

	/* increase the snake size... append to linked list */
	void grow_snake(struct snake *);

	/* recursivly free allocated snake memory */
	void recursive_snake_destroy(struct snake_part *);

	/* call recursive_snake_destroy and then free snake container */
	void destroy_snake(struct snake *);

	/* add_part -- allows recursive behaviour of growing snake */
	void add_part(struct snake_part *);

/* place all items on the field */
void place_all(char * pfield[__SIZE][__SIZE], struct snake_part * , struct point *);

/* decrease the wait time */
void speed_up(unsigned *);

/* return 1 if snake should die. 0 if it should live. --die if touching self */
int die_on_tail_touch(struct snake * james);

// ----------------opengl specific functions 
	void __opengl_snake_update_func(void);
	void IDLE(void);
	void key_pressed (unsigned char,int,int);
	void s_key_pressed(int,int,int);
	


/* I N L I N E   F U N C T I O N S */

/* place point at random location */
static inline void randomize_point(struct point * thepoint){
	thepoint->x = rand()%__SIZE;
	thepoint->y = rand()%__SIZE;
}

/* get user input for controlling the snake */
static inline void get_game_controls(struct settings _settings){
	extern char direction;
	int disable_reverse = 1; // add controlability from menu
	int keycode;

	keycode = wgetch(stdscr);

	if (disable_reverse){ // disable the ability to kill the snake by reversing ultra shaply
		if (((keycode == 'w')||(keycode == KEY_UP))&& (direction == 'S')){
			return;// ignore input
		}
		if (((keycode == 's')||(keycode == KEY_DOWN))&& (direction == 'N')){
			return;// ignore input
		}
		if (((keycode == 'a')||(keycode == KEY_LEFT))&& (direction == 'E')){
			return;// ignore input
		}
		if (((keycode == 'd')||(keycode == KEY_RIGHT))&& (direction == 'W')){
			return;// ignore input
		}
	}

	switch (keycode){
		case KEY_UP:
		case 'w':
			direction = 'N';
			break;
		case KEY_DOWN:
		case 's':
			direction = 'S';
			break;			
		case KEY_LEFT:
		case 'a':
			direction = 'W';
			break;
		case KEY_RIGHT:
		case 'd':
			direction = 'E';
			break;
		case 'q':
		case 'Q':
			direction = 'Q';
			break;	
		default:
			direction = direction;
		};//close switch
};

#define SNAKE_PAIR 5
#define FOOD_PAIR  6
#define EMPTY_PAIR 7
/* display the field */
static inline void show_field(char * pfield[__SIZE][__SIZE]){ // update to draw new head and erase old tail food aswell

	static short first_time_this_function_was_called = 1;
	if (first_time_this_function_was_called){	// on first call to this func erase the previous junk
		clear();								// only do on first time to avoid flickering
		first_time_this_function_was_called = 0;
	}
	for (int i = 0; i < 50; i++){
		for (int k = 0; k < 50; k++){
			if (pfield[i][k] == BLACK_SQUARE){
				attron(COLOR_PAIR(SNAKE_PAIR));
			}else if (pfield[i][k] == FOOD_ICON){
				attron(COLOR_PAIR(FOOD_PAIR));
			}else if (pfield[i][k] == WHITE_SQUARE){
				attron(COLOR_PAIR(EMPTY_PAIR));
			}
			printw("%*s",4,pfield[i][k]);
			attron(COLOR_PAIR(1));
		}
		printw("\n");
	}
};

static inline void show_field_opengl(char * pfield[__SIZE][__SIZE]){
	static int first_run_do_setup = 1;
	if (first_run_do_setup){
		// do set up
		
		// set static int value to signal not to do setup again
		first_run_do_setup = 0;
	}
	// refresh opengl window with current state of pfield
}

static inline void show_field_no_curs(char * pfield[50][50]){
	// turn off ncurses
	wmove(stdscr, 0, 0);
	clear();
	endwin();	
	for (int i = 0; i < 50; i++){
		for (int k = 0; k < 50; k++){
			printf("%*s",4,pfield[i][k]);
		}
		printf("\n");
	}
}

/* fill the field with default value */
static inline void fill_field(char * pfield[50][50], char * filler){
	for (int i = 0; i < 50; i++){
		for (int k = 0; k < 50; k++){
			pfield[i][k] = filler;
		}
	}
}

/* save score to file */
static inline void save_score(char * plyrname , unsigned score, char * sfname){
	time_t now;
	time(&now);
	FILE*scorefile = fopen(sfname, "a");
	fprintf(scorefile, "Name: %s Score: %d Date: %s\n", plyrname, score, ctime(&now));
	fclose(scorefile);
}

static inline int center_text(const char * string, int side_len){
	int string_size = 0;
	for (; string[string_size] != '\0'; string_size++);
//	fprintf(stderr, "%d\n", string_size);
//	return (side_len/2)-(string_size/2);
	return (side_len - (string_size/2))/2;
}

static inline void set_sleep_time(struct settings _settings, unsigned * sleep_time){
	switch (_settings.hardness){ // change sleep time (speed of game) based on hardness setting	
		case 1:
			*sleep_time = 50000; // easy
			break;
		case 2: 
			*sleep_time = 30000; // medium
			break;
		case 3:
			*sleep_time = 10000; // hard
			break;
		default:
			break; 	// make no changes to sleep_time
	};
}

static inline void turn_off_ncurses(int turn_fully_off){
	nodelay(stdscr, FALSE);
	echo();
	wmove(stdscr, 0, 0);
	clear();
	if (turn_fully_off){
		endwin();
	}
}

static inline void collect_name_save_score(unsigned _score, char _name[4]){
	printf("ENTER YOUR NAME: ");
	scanf("%3s", _name);
	save_score(_name, _score, "scorelog.txt");
}

static inline unsigned random_apple_type(void){
#define NUMBER_OF_TYPES_OF_APPLES 2
	return rand()%NUMBER_OF_TYPES_OF_APPLES;
}

static inline unsigned random_apple_shade(void){
   return rand()%(255 - 155) + 155;
}   

