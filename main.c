#include "snake.h"
#include "music_player.h"

#ifdef DEBUG
#include "stubber.h"
#endif


// place the snake and the food on the field
void place_all(char * pfield[__SIZE][__SIZE], struct snake_part * part, struct point * food){
	pfield[food->x][food->y] = FOOD_ICON;
	struct snake_part * curnt = part;
	while(curnt != NULL){
		pfield[curnt->place.x][curnt->place.y] = BLACK_SQUARE;
		curnt = curnt->next;
	}
}

/*
 *	@function	speed_up
 *	@author		William Doyle
 *	@date		pre pebuary 27th 2020
 *	@arguments	unsigned ptr
 *	@returns	nothing
 *	@desc		increase sleep_time if doing so would keep it 
 *				in bounds
 * */
void speed_up(unsigned * sleep_time){
	if (*sleep_time < 100){
		return; //decrease speed no further
	}
	else {
		*sleep_time -= 500;
	}
}

int die_on_tail_touch(struct snake * james){
	struct point * locs;
	int number_of_snake_parts = 0;
	int should_die = 0;
	struct snake_part * curnt;
	
	for (struct snake_part * part_index = james->head; part_index != NULL; part_index = part_index->next){
		++number_of_snake_parts;
	}
	
	curnt = james->head;//reset
	locs = malloc (number_of_snake_parts*sizeof(struct point));
	check_malloc(locs, __func__);
	for (int i = 0; i < number_of_snake_parts; i++){
		locs[i] = curnt->place;
		curnt = curnt->next;
		if (curnt == NULL){
			break;
		}
	}//close for i
	for (int i = 0; i < number_of_snake_parts; i++){
		for (int k = 0; k < number_of_snake_parts; k++){
			if ((i != k)&&((locs[i].x == locs[k].x)&&(locs[i].y == locs[k].y))){ //snake touching self
				should_die = 1;
				goto EXIT; // K&R 3.8
			}//close if
		}//close for k
	}//close for i
EXIT:
	free(locs);
	return should_die;
}


static inline void setup_opengl_mode(int argc, char ** argv){
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE);
	glutInitWindowSize(700, 700);
	glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);
	glutCreateWindow("James The Snake"); // create window with title
	glutKeyboardFunc(key_pressed);
	glutSpecialFunc(s_key_pressed);
	glClear(GL_COLOR_BUFFER_BIT);
	glClearColor(255.0f/255.0f, 255.0f/255.0f, 255.0f/255.0f, 1.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glutDisplayFunc(__opengl_snake_update_func);
	glutIdleFunc(IDLE);
	glutMainLoop();	
	return;
}

int main (int argc, char ** argv){
	extern char direction;
	extern char * field [__SIZE][__SIZE];
	direction = 'W';		// start with snake moving west 
	extern struct snake * james;
	extern struct point * food;
	food = malloc(sizeof(struct point));
	check_malloc(food, __func__);
	james = make_snake(0,0);

	extern unsigned player_score;
	player_score = 0;
	extern unsigned sleep_time;
	sleep_time = 50000;
	
	char player_name [4];
//	srand(time(NULL));
	srand(0x67872); 
	randomize_point(food);

	extern unsigned short apple_type;
	extern unsigned short apple_shade_extent;	
	
	/* ncurses setup */
	setlocale(P_ALL, "");
	initscr();
	cbreak();
	noecho();
	start_color();
	if (has_colors()){
		init_pair(1, COLOR_BLACK, COLOR_YELLOW);
		init_pair(FOOD_PAIR, COLOR_RED, COLOR_GREEN);
		init_pair(SNAKE_PAIR, COLOR_GREEN, COLOR_BLUE);
		init_pair(EMPTY_PAIR, COLOR_WHITE, COLOR_WHITE);
		wbkgd(stdscr, COLOR_PAIR(1));					/* set background color */
	}

	keypad(stdscr, TRUE);
	/* menu */
	extern struct settings _settings;
	_settings = game_menu(); 	// ask user for difficulty and graphics settings via menu interface

#ifndef NO_MUSIC
#define PATH_TO_IN_GAME_MUSIC "music/Mid-Air_Machine_-_Knowing_Nothing_140__44.wav"
	struct music_player * game_music = make_music_player(PATH_TO_IN_GAME_MUSIC, 44100, 2, 221 );
	game_music->start(game_music);
#endif

	set_sleep_time(_settings, &sleep_time);
		
	extern void (*show_all)(char * [__SIZE][__SIZE]);
	show_all = show_field;

	switch (_settings.graphics_mode){ // use funcptrs to swap graphics
		case 1:																// unicode / no nucurses
			show_all = show_field_no_curs;
			break;
		case 2:																// unicode + ncurses
			show_all = show_field;
			break;
		case 3:																// opengl  
			apple_type = random_apple_type();
			apple_shade_extent = random_apple_shade();
			turn_off_ncurses(1);
			setup_opengl_mode(argc, argv);

			collect_name_save_score(player_score, player_name);
			goto END_MAIN;	
			break;
		default:
			fprintf(stderr, "Bad graphics setting! Issue caught in %s\n", __func__);
			break; 
	};
	
	nodelay(stdscr, TRUE); // dont wait for users input... just keep moving in the same direction
	fill_field(field, WHITE_SQUARE);
	show_all(field);

	do{	/* game loop */
		wmove(stdscr, 0, 0);
		refresh();
		fill_field(field, WHITE_SQUARE);
		get_game_controls(_settings);
		move_snake(james);
		if (die_on_tail_touch(james)){
			break;
		}
		if ((james->head->place.x == food->x) && (james->head->place.y == food->y)){
			player_score++;
			speed_up(&sleep_time);
			grow_snake(james);
			randomize_point(food);
		}			
		place_all(field, (james->head), food);
		show_all(field);
		usleep(sleep_time);	
	}while(direction != 'Q');

	turn_off_ncurses(1);
	collect_name_save_score(player_score, player_name);

END_MAIN:

#ifndef NO_MUSIC	
	game_music->stop(game_music);
	usleep(10000);
	destroy_music_player(game_music);
#endif
	free(food);
	destroy_snake(james);
	exit(EXIT_SUCCESS);
};
