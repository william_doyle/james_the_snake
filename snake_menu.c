#include "snake.h"
#include <ncurses.h>
#include <pthread.h>
#include <signal.h>
#include "music_player.h"

#define TEXT_PAIR	12	/* colors for big text */
#define NORM_PAIR	13	/* colors for background */
#define INPUT_PAIR	14	/* colors for input fields */

#define NUMBER_OF_GRAPHICS_OPTIONS 3
#define NUMBER_OF_HARDNESS_OPTIONS 3
#define INSTR_PAIR 19
#define SUB_TITLE_PAIR 21
#define DEFAULT_HARDNESS 2
#define DEFAULT_GRAPHICS_MODE 2



int map_in_word (int x, int y){
#define YOFF 7 // y axis offset of text (ncurses reverses x and y)
#define XOFF -2
	/* return 1 if cell at xy is a part of our word.. otherwise return false */

	/* J */	
	if ((x == 10+XOFF ) && (y == 5+YOFF)) return 1;
	if ((x == 5+XOFF ) && (y == 5+YOFF)) return 1;
	if ((x == 6+XOFF ) && (y == 5+YOFF)) return 1;
	if ((x == 7+XOFF ) && (y == 5+YOFF)) return 1;
	if ((x == 8+XOFF ) && (y == 5+YOFF)) return 1;
	if ((x == 9+XOFF ) && (y == 5+YOFF)) return 1;
	if ((x == 10+XOFF ) && (y == 5+YOFF)) return 1;
	if ((x == 10+XOFF ) && (y == 3+YOFF)) return 1;
	if ((x == 11+XOFF ) && (y == 4+YOFF)) return 1;
	
	/* A */
	if ((x == 8+XOFF ) && (y == 9+YOFF)) return 1;	/* 'A's bar */	
	if ((x == 5+XOFF ) && (y == 9+YOFF)) return 1;	
	if ((x == 6+XOFF ) && (y == 8+YOFF)) return 1;	
	if ((x == 7+XOFF ) && (y == 8+YOFF)) return 1;	
	if ((x == 8+XOFF ) && (y == 8+YOFF)) return 1;	
	if ((x == 9+XOFF ) && (y == 8+YOFF)) return 1;	
	if ((x == 10+XOFF ) && (y == 8+YOFF)) return 1;	
	if ((x == 6+XOFF ) && (y == 10+YOFF)) return 1;	
	if ((x == 7+XOFF ) && (y == 10+YOFF)) return 1;	
	if ((x == 8+XOFF ) && (y == 10+YOFF)) return 1;	
	if ((x == 9+XOFF ) && (y == 10+YOFF)) return 1;	
	if ((x == 10+XOFF ) && (y == 10+YOFF)) return 1;

	/* m */
	if ((x == 5+XOFF ) && (y == 13+YOFF)) return 1;	
	if ((x == 6+XOFF ) && (y == 13+YOFF)) return 1;	
	if ((x == 7+XOFF ) && (y == 13+YOFF)) return 1;	
	if ((x == 8+XOFF ) && (y == 13+YOFF)) return 1;	
	if ((x == 9+XOFF ) && (y == 13+YOFF)) return 1;	
	if ((x == 10+XOFF ) && (y == 13+YOFF)) return 1;	
	if ((x == 5+XOFF ) && (y == 17+YOFF)) return 1;	
	if ((x == 6+XOFF ) && (y == 17+YOFF)) return 1;	
	if ((x == 7+XOFF ) && (y == 17+YOFF)) return 1;	
	if ((x == 8+XOFF ) && (y == 17+YOFF)) return 1;	
	if ((x == 9+XOFF ) && (y == 17+YOFF)) return 1;	
	if ((x == 10+XOFF ) && (y == 17+YOFF)) return 1;	
	if ((x == 7+XOFF ) && (y == 15+YOFF)) return 1;	
	if ((x == 8+XOFF ) && (y == 15+YOFF)) return 1;	
	if ((x == 6+XOFF ) && (y == 16+YOFF)) return 1;	
	if ((x == 6+XOFF ) && (y == 14+YOFF)) return 1;

	/* E */
	if ((x == 5+XOFF ) && (y == 20+YOFF)) return 1;	
	if ((x == 5+XOFF ) && (y == 21+YOFF)) return 1;	
	if ((x == 5+XOFF ) && (y == 22+YOFF)) return 1;	
	if ((x == 10+XOFF ) && (y == 20+YOFF)) return 1;	
	if ((x == 10+XOFF ) && (y == 21+YOFF)) return 1;	
	if ((x == 10+XOFF ) && (y == 22+YOFF)) return 1;	
	
	if ((x == 6+XOFF ) && (y == 20+YOFF)) return 1;	
	if ((x == 7+XOFF ) && (y == 20+YOFF)) return 1;	
	if ((x == 8+XOFF ) && (y == 20+YOFF)) return 1;	
	if ((x == 9+XOFF ) && (y == 20+YOFF)) return 1;	
	if ((x == 7+XOFF ) && (y == 21+YOFF)) return 1;	
	if ((x == 7+XOFF ) && (y == 22+YOFF)) return 1;	
	
	/* S */	
	if ((x == 6+XOFF ) && (y == 25+YOFF)) return 1;	
	if ((x == 5+XOFF ) && (y == 26+YOFF)) return 1;	
	if ((x == 5+XOFF ) && (y == 27+YOFF)) return 1;	
	if ((x == 10+XOFF ) && (y == 26+YOFF)) return 1;	
	if ((x == 7+XOFF ) && (y == 26+YOFF)) return 1;	
	if ((x == 10+XOFF ) && (y == 27+YOFF)) return 1;	
	if ((x == 8+XOFF ) && (y == 27+YOFF)) return 1;	
	if ((x == 9+XOFF ) && (y == 25+YOFF)) return 1;	
	if ((x == 9+XOFF ) && (y == 28+YOFF)) return 1;	
	if ((x == 6+XOFF ) && (y == 28+YOFF)) return 1;	
//	if ((x == 7) && (y == 25+YOFF)) return 1;	
//	if ((x == 8) && (y == 28+YOFF)) return 1;	

	return false;
}

void increase_graphics_settings(struct settings * _settings){
	if (_settings->graphics_mode < NUMBER_OF_GRAPHICS_OPTIONS){
		_settings->graphics_mode += 1;
	}
}

void decrease_graphics_settings(struct settings * _settings){
	if (_settings->graphics_mode > 1){
		_settings->graphics_mode -= 1;
	}
}

char * translate_graphics_setting_to_string(short gsetting){
	switch (gsetting){
		case 1:
			return "PLAN TEXT";
		case 2:
			return "NCURSES";
		case 3:
			return "OPENGL";
		default:
			return "UNKNOWN";
	};
}

char * translate_hardness_setting_to_string(short hsetting){
	switch (hsetting){
		case 1:
			return "Easy";
		case 2:
			return "Medium";
		case 3:
			return "Hard";
		default:
			return "UNKNOWN";
	};
}

void increase_hardness_setting(struct settings * _settings){
	if (_settings->hardness < NUMBER_OF_HARDNESS_OPTIONS){
		_settings->hardness += 1;
	}
}

void decrease_hardness_setting(struct settings * _settings){
	if (_settings->hardness > 1){
		_settings->hardness -= 1;
	}
}

void draw_settings_menu(struct settings _settings){
#define TXT_BX_OFST	12 // text box offset
	wmove(stdscr, 0,0 );
	for (int i = 0; i < 50; i++){
		for (int k = 0; k < 50; k++){
			if (((i>12)&&(i<20)&&(k>5)&&(k<40)) || ((i>22)&&(i<30)&&(k>5)&&(k<40))){
				attron(COLOR_PAIR(INPUT_PAIR));
			}else if (map_in_word(i,k)){
				attron(COLOR_PAIR(TEXT_PAIR));
			}else {
				attron(COLOR_PAIR(NORM_PAIR));
			}
			refresh();
			printw("%*s",4, BLACK_SQUARE);
		}
		printw("\n");
	}
	
	attron(COLOR_PAIR(SUB_TITLE_PAIR));// white_on_blue
	wmove(stdscr, 10, 28);
	printw("THE GAME OF ROMANCE, CONQUEST, AND SNAKES!");

	attron(COLOR_PAIR(INSTR_PAIR));
	
#define __SCRWIDTH 75
	const char * contrl_hrdnes = "LEFT + RIGHT ARROWS CONTROL DIFICULTY";
	wmove(stdscr,14, center_text(contrl_hrdnes, __SCRWIDTH));
	printw(contrl_hrdnes);

	const char * contrl_grphc = "UP + DOWN ARROWS CONTROL GRAPHICS";
	wmove(stdscr,24, center_text(contrl_grphc, __SCRWIDTH ));
	printw(contrl_grphc);

	// show graphics
	wmove(stdscr, 26, center_text(translate_graphics_setting_to_string(_settings.graphics_mode), __SCRWIDTH));
	printw("%s", translate_graphics_setting_to_string(_settings.graphics_mode));

	// show dificulty
	wmove(stdscr, 16, center_text(translate_hardness_setting_to_string(_settings.hardness), __SCRWIDTH));
	printw("%s", translate_hardness_setting_to_string(_settings.hardness));
	
	refresh();
}	

struct settings game_menu(){
	init_pair(INSTR_PAIR, COLOR_WHITE, COLOR_BLACK);
	init_pair(TEXT_PAIR, COLOR_WHITE, COLOR_WHITE);
	init_pair(NORM_PAIR, COLOR_BLUE, COLOR_BLUE);
	init_pair(INPUT_PAIR, COLOR_RED, COLOR_RED);
	init_pair(SUB_TITLE_PAIR, COLOR_WHITE, COLOR_BLUE);

	struct settings _settings = {DEFAULT_HARDNESS, DEFAULT_GRAPHICS_MODE};
	draw_settings_menu(_settings);

	int choice;

#ifndef NO_MUSIC
	struct music_player * menu_music = make_music_player("music/Mid-Air_Machine_-_A_State_of_Despair_93__34.wav", 44100, 2, 95);
	menu_music->start(menu_music);
#endif

	do {
		draw_settings_menu(_settings);
		choice = wgetch(stdscr);
		switch (choice){
			case KEY_UP:
				//increase graphics
				increase_graphics_settings(&_settings); 
				break;
			case KEY_DOWN:
				//decrease graphics
				decrease_graphics_settings(&_settings);
				break;
			case KEY_RIGHT:
				//increase difficulty
				increase_hardness_setting(&_settings);
				break;
			case KEY_LEFT:
				//decrease difficulty
				decrease_hardness_setting(&_settings);
				break;
			case '\n':
			case 'q':
			case 'Q':
				choice = 'q';
				menu_music->stop(menu_music);
		};
	} while (choice != 'q');

	/* return settings */
#ifndef NO_MUSIC
	menu_music->stop(menu_music);
	fprintf(stderr, "Loading...\n");
	destroy_music_player(menu_music);
#endif
	return _settings;
}
