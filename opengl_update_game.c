#include "snake.h"
#include <math.h>
#include <pthread.h>

void s_key_pressed(int key , int x, int y){

//	fprintf(stderr, "%d\n", key);
	// ^ 101
	// > 102
	// V 103
	// < 100
	
	unsigned char pass_key;
	if (key == 101){
		pass_key = 119;
	} else if (key == 102){
		pass_key = 100;
	} else if (key == 103){
		pass_key = 115;
	}else if (key == 100){
		pass_key = 97;
	}
	key_pressed(pass_key, x, y);
}

void key_pressed (unsigned char key, int x, int y){
#define __w 97
#define __d 119
#define __s 100
#define __a 115
	extern char direction;
	int disable_reverse = 1;

//	fprintf(stderr, "%c\n", key);

	if (disable_reverse){ // disable the ability to kill the snake by reversing ultra shaply
		if ((key == __w)&& (direction == 'S')){
			return;// ignore input
		}
		if ((key == __s)&& (direction == 'N')){
			return;// ignore input
		}
		if ((key == __a)&& (direction == 'E')){
			return;// ignore input
		}
		if ((key == __d)&& (direction == 'W')){ 
			return;// ignore input
		}
	}
	// w 119
	// d 100
	// s 115
	// a 97
	// q 113
	switch (key){
		case __w:
			direction = 'N';
			break;
		case __d:
			direction = 'E';
			break;
		case __s: 
			direction = 'S';
			break;
		case __a:
			direction = 'W';
			break;
		case 113:
			direction = 'Q';
			break;
		default:
			direction = direction;
	};
}

// rotate the original field and place rotated copy into f_copy
static inline void copy_rotate_field(char * f_original[__SIZE][__SIZE], char * f_copy[__SIZE][__SIZE]){
	for (int i = 0; i < __SIZE/2; i++){	// rotate for opengl display
		for (int j = i; j < __SIZE-i-1; j++){
			char * temp = f_original[i][j];
			f_copy[i][j] = f_original[__SIZE - 1 - j][i];
			f_copy[ __SIZE  - 1 - j][i] = f_original[__SIZE - 1 - i][__SIZE  -1 - j];
			f_copy[__SIZE -  1 - i][__SIZE - 1 - j] = f_original[j][__SIZE -1 - i];
			f_copy[j][__SIZE  - 1 - i] = temp;
		}
	}
}


void draw_snake_cell(struct snake * james, int i, int k, int t_siz){
//	#define FULL_FANCY
//	#define NO_FANCY
#ifdef FULL_FANCY
	#define HEAD_WEDGE
	#define BODY_WEDGE
	#define DRAW_EYE
#endif
#define DRAW_EYE
	glColor3ub( 0.0f , 0.0f , 0.0f ); // black -- body color
	glRectf(t_siz*k, t_siz*i, (t_siz*k)+t_siz, (t_siz*i)+t_siz); // draw cell sized squares
#ifdef NO_FANCY
	return;
#endif
	// draw tiny triangle pointing in direction of travel
	// head point in direction of global direction
	// all other nodes point to previous node

	if ((james->head->place.x == k )&&(james->head->place.y == i)){
#ifdef DRAW_EYE	
		glColor3ub( 255.0f , 25.0f , 25.0f );	// eye color
		glRectf(t_siz*k+(t_siz/2), t_siz*i+(t_siz/2), (t_siz*k)+t_siz-(t_siz/4), (t_siz*i)+t_siz-(t_siz/4));
#endif	
		glColor3ub( 250.0f , 50.0f , 50.0f );
#ifdef HEAD_WEDGE 
		glBegin(GL_TRIANGLES);
			glVertex3f(t_siz*k, t_siz*i, 0);
			glVertex3f((t_siz*k)+t_siz,  (t_siz*i), 0);
			glVertex3f((t_siz*k),  (t_siz*i)+t_siz, 0);
		glEnd();
#endif
	} else {
		glColor3ub( 150.0f , 50.0f , 50.0f );
	}
#ifdef BODY_WEDGE 
		glBegin(GL_TRIANGLES);
			glVertex3f(t_siz*k, t_siz*i, 0);
			glVertex3f((t_siz*k)+t_siz,  (t_siz*i), 0);
			glVertex3f((t_siz*k),  (t_siz*i)+t_siz, 0);
		glEnd();
#endif


}

void* play_sound(){
	system("aplay music/bite1.wav" );
}

void __opengl_snake_update_func(void){
	
	extern unsigned short apple_type;
	extern unsigned short apple_shade_extent;
	extern char * field [__SIZE][__SIZE];
	extern struct snake * james;
	extern struct point * food;
	extern void (*show_all)(char * [__SIZE][__SIZE]);
	extern unsigned player_score;
	extern int food_pos[2];
	int height_of_window = glutGet(GLUT_WINDOW_HEIGHT);
	int width_of_window = glutGet(GLUT_WINDOW_WIDTH);	
#ifndef NO_SFX
	static pthread_t sound_thread;
#endif
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_PROJECTION);		
	glLoadIdentity();
	glOrtho(0, width_of_window, 0, height_of_window, -1, 1);
	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();
	glTranslatef( 0.5, 0.5, 0 );
	
	fill_field(field, WHITE_SQUARE);// remove and instead draw new head and erase old tail
	show_all(field);
	
	move_snake(james);
	if (die_on_tail_touch(james)){
		glutLeaveMainLoop();
	}
	if ((james->head->place.x == food->x) && (james->head->place.y == food->y)){
#ifndef NO_SFX
		pthread_create(&sound_thread, NULL, play_sound, NULL);
#endif
		player_score++;
		speed_up(&sleep_time);
		grow_snake(james);
		randomize_point(food);
		apple_type = random_apple_type();
		apple_shade_extent = random_apple_shade();
	}			
	struct snake_part * curpt = james->head;
	while (curpt != NULL){
		field[curpt->place.x][curpt->place.y] = BLACK_SQUARE;
		curpt = curpt->next;
	}
	field[food->x][food->y] = FOOD_ICON;
	show_all(field);
	usleep(sleep_time);	
	
#define TILE_SIZE width_of_window/__SIZE
	for (int i = 0; i < __SIZE; i++){	// draw square in position based on i k of one color
		for (int k = 0; k < __SIZE; k++){
			if (field[k][i] == BLACK_SQUARE){ // snake
				draw_snake_cell(james, i, k, TILE_SIZE);
				continue;
			} 
			else if (field[k][i] == WHITE_SQUARE){ // background
				glColor3ub( 208.0f , 180.0f , 89.0f );
			}
		   	else if (field[k][i] == FOOD_ICON){ // food
				glColor3ub( 208.0f , 180.0f , 89.0f );
				glRectf(TILE_SIZE*k, TILE_SIZE*i, (TILE_SIZE*k)+TILE_SIZE, (TILE_SIZE*i)+TILE_SIZE);
				switch(apple_type){
					case 0:	// red apple
						glColor3ub( apple_shade_extent , 0.0f , 0.0f );
						break;	
					case 1: // green apple
						glColor3ub( 0.0f , apple_shade_extent, 0.0f );
						break;	
				};
				// draw circle with brown stem
				// source: https://community.khronos.org/t/how-to-draw-circle/59661/4
				const int resolution = 20;
				const double radius = TILE_SIZE/2;
				glBegin(GL_POLYGON);
					for (int q = 0; q < 360; q += 360/resolution){
						double heading = q * 3.14159265358979232 / 180;
						glVertex2d(cos(heading) * radius +((TILE_SIZE*k)+(TILE_SIZE/2.01)), sin(heading) * radius+((TILE_SIZE*i)+(TILE_SIZE/2.01)));
					}
				glEnd();
				glFlush();
				continue;

			}
			else {
				glColor3ub( 100.0f, 0.0f , 100.0f ); // "error cell" color
			}
			glRectf(TILE_SIZE*k, TILE_SIZE*i, (TILE_SIZE*k)+TILE_SIZE, (TILE_SIZE*i)+TILE_SIZE);
		}
	}
	glutSwapBuffers();
}


void IDLE (void){
	glutPostRedisplay();
}

